//
//  City.swift
//  WeatherSwift
//
//  Created by Alex on 25.01.17.
//  Copyright © 2017 Alex. All rights reserved.
//

import Foundation
import UIKit

class City {
    
    var nameCity: String = ""
    var temp: Double = 0
    var descriptionWeather: String = ""
    var icon: UIImage?
    
    init(dict: NSDictionary) {
        
        if let name = dict["name"] as? String {
            nameCity = name
        }
        if let main = dict["main"] as? NSDictionary {
            if let temp = main["temp"] as? Double {
                self.temp = temp - 273.15 //cause we need Celcius
            }
        }
        
        if let weather = dict["weather"] as? NSArray {
            if let weatherZero = weather[0] as? NSDictionary {
                descriptionWeather = weatherZero["description"] as? String ?? ""
                if let iconName = weatherZero["icon"] as? String {
                    icon = UIImage(named: iconName) 
                }
            }
        }
    }
}
