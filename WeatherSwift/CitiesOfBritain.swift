//
//  CitiesOfBritain.swift
//  WeatherSwift
//
//  Created by Alex on 31.01.17.
//  Copyright © 2017 Alex. All rights reserved.
//

import Foundation

let urlCityNames = "http://raw.githubusercontent.com/David-Haim/CountriesToCitiesJSON/master/countriesToCities.json"
let amountOfCitiesInBunch = 15

class CitiesOfBritain {
    
    static func getCitiesList() -> [String] {
        
        let citiesNamesUrl = URL(string: urlCityNames)
        let citiesNamesData = NSData(contentsOf: citiesNamesUrl!)
        var citiesNamesJson : NSDictionary
        
        do {
            citiesNamesJson = try JSONSerialization.jsonObject(with: citiesNamesData! as Data, options: []) as! NSDictionary
            let citiesArray = citiesNamesJson["United Kingdom"] as? [String]
            return citiesArray ?? ["error"]
        }
        catch {
            print(error)
            return ["error"]
        }
    }
    
    static func getRandomCities() -> [String] {
        
        let listCitiesArray = getCitiesList()
        var randomCites = [String]()
        var i = 0
        while i < amountOfCitiesInBunch {
            i += 1
            let randomCity = Int(arc4random_uniform(UInt32(Int(listCitiesArray.count))))
            randomCites.append(listCitiesArray[randomCity])
        }
        return randomCites
    }
}
