//
//  AuthViewController.swift
//  WeatherSwift
//
//  Created by Alex on 29.01.17.
//  Copyright © 2017 Alex. All rights reserved.
//


import VK_ios_sdk
import UIKit

let VK_APP_ID = "5791612"

class AuthViewController: UIViewController, VKSdkDelegate, VKSdkUIDelegate  {
    
    let SCOPE = [VK_PER_WALL]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // start of work of SDK, smth like a Shared Instance
        
        let sdkInstance = VKSdk .initialize(withAppId: VK_APP_ID)
        sdkInstance?.register(self)
        sdkInstance?.uiDelegate = self
        
        // VKSdk.forceLogout() // if you need to logout
        
        // start session, and if user is not authorised yet - then we show screen of authorize (native VC), other way - go to next View Controller for work
        
        VKSdk.wakeUpSession(SCOPE) { (state, error) in
            
            if state == VKAuthorizationState.authorized {
                print("access granted")
                self.startWorking()
            } else  {
                print(error ?? "not authorized")
            }
        }
    
    }
    
    func startWorking() {
        self.performSegue(withIdentifier: "SEQUE_START", sender: self)
    }
    
    //MARK: - Actions
    
    @IBAction func authorize(_ sender: UIButton) {
        
        // get new access token
        
        VKSdk.authorize(SCOPE)
        
    }

    //MARK: - @protocol VKSdkDelegate
    
    func vkSdkTokenHasExpired(_ expiredToken: VKAccessToken!) {
        //self.authorize(nil)
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        if result.token != nil {
            self.startWorking()
        } else if result.error != nil{
            print("access denied")
        }
        
    }
    
    func vkSdkUserAuthorizationFailed() {
        print("access denied")
        
        let _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - @protocol VKSdkUIDelegate
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        let vc = VKCaptchaViewController.captchaControllerWithError(captchaError)
        vc?.present(in: self.navigationController?.topViewController)
        print("please type text from picture")
    }
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        print("protocol VKSdkUIDelegate vkSdkShouldPresentViewController")
        self.navigationController?.topViewController?.present(controller, animated: true, completion: nil)
    }
    
}
