//
//  ListCitiesVC.swift
//  WeatherSwift
//
//  Created by Alex on 28.01.17.
//  Copyright © 2017 Alex. All rights reserved.
//

import UIKit
import ALLoadingView


class ListCitiesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var citiesArray = [City]()
    var countAnimatedCells: Int = 0
    var dataIsLoading: Bool = false
    var currentRow: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBunchRandomCities()
    }

    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.currentRow = indexPath.row
        alertView {
            self.postOnWall()
        }
    }
    
    // MARK: - Table view data source
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.citiesArray.count
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath) as! CityCell
        let city = self.citiesArray[indexPath.row]
        cell.makeCell(indexPathRow: indexPath.row, city: city)
        if indexPath.row >= self.countAnimatedCells {
            self.countAnimatedCells = indexPath.row
            Utils.makeAnimation(cell: cell)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let row = indexPath.row
        if self.dataIsLoading == false && row == (self.citiesArray.count - 1) {
            self.addBunchRandomCities()
        }
    }
 
    //MARK: Private methods
    func postOnWall () {
        let city = self.citiesArray[self.currentRow]
        let message = "in the city " + city.nameCity + "of the United Kingdom current weather is - " + String(format: "%.2f", city.temp) + " " + city.descriptionWeather
        ApiManager.wallPostWithMessage(message: message) { () -> (Void) in
            print("success")
        }
    }
    
    func addBunchRandomCities() {
        
        self.dataIsLoading = true
        ALLoadingView.manager.showLoadingView(ofType: .basic, windowMode: .windowed)
        DispatchQueue.global(qos: .background).async {
            
            ApiManager.getForecastForBunchCities(closureSucces: { (listCities) in
                self.citiesArray.append(contentsOf: listCities)
                var newPaths = [IndexPath]()
                let fromInt = self.citiesArray.count - listCities.count
                for i in fromInt..<self.citiesArray.count {
                    newPaths.append(IndexPath(row: i, section: 0))
                }
                DispatchQueue.main.async {
                    ALLoadingView.manager.hideLoadingView()
                    
                    self.tableView.beginUpdates()
                    self.tableView.insertRows(at: newPaths, with: UITableViewRowAnimation.fade)
                    self.tableView.endUpdates()
                    self.dataIsLoading = false
                    self.tableView.scrollToRow(at: newPaths[0], at: UITableViewScrollPosition.middle, animated: true)
                }
            }, closureFailure: {
                DispatchQueue.main.async {
                ALLoadingView.manager.hideLoadingView()
                self.dataIsLoading = false
                }
            })
        }
    }
}

extension UIViewController {
    
    func alertView(closure: @escaping () -> (Void) ) {
        let alertController = UIAlertController(title: nil, message: "post info on VK wall?", preferredStyle: .actionSheet)
        let postAction = UIAlertAction(title: "YES", style: .default) { (action) in
            closure()
        }
        let defaultAction = UIAlertAction(title: "NO", style: .default, handler: nil)
        alertController.addAction(postAction)
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }
}
