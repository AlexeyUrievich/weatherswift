//
//  AppDelegate.swift
//  WeatherSwift
//
//  Created by Alex on 29.01.17.
//  Copyright © 2017 Alex. All rights reserved.
//

import UIKit
import VK_ios_sdk

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let app = options[.sourceApplication] as? String
        
        VKSdk.processOpen(url, fromApplication: app)
        
        return true
    }
}


