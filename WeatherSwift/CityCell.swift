//
//  CityCell.swift
//  WeatherSwift
//
//  Created by Alex on 26.01.17.
//  Copyright © 2017 Alex. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Functions

    func makeCell(indexPathRow: Int, city: City){

        textLabel?.text = city.nameCity + " - " + String(format: "%.2f", city.temp)
        let font = UIFont(name: "Arial", size: 14)
        textLabel?.font = font
        textLabel?.numberOfLines = 2
        textLabel?.adjustsFontSizeToFitWidth = true
        textLabel?.textColor = UIColor.black
        detailTextLabel?.font = UIFont(name: "Arial", size: 12)
        detailTextLabel?.textColor = UIColor.black
        detailTextLabel?.text = city.descriptionWeather
        detailTextLabel?.numberOfLines = 2
        textLabel?.adjustsFontSizeToFitWidth = true
        imageView?.image = city.icon
    }
}

