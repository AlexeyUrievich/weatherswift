//
//  ApiManager.swift
//  WeatherSwift
//
//  Created by Alex on 28.01.17.
//  Copyright © 2017 Alex. All rights reserved.
//

import Foundation
import VK_ios_sdk
import ReachabilitySwift

let urlOpenWeatherMap = "http://api.openweathermap.org/data/2.5/weather?q"
let apiKey = "&APPID=8e100450075e4d9ec4c3a556e4d96fb4"
let reachability = Reachability()!

class ApiManager {
   
    static func getForecastForBunchCities(closureSucces: ([City]) -> Void, closureFailure: () -> Void) {
        
        if reachability.isReachable {
            
            let listCitiesArray = CitiesOfBritain.getRandomCities()
            
            var newBunchOfCitiesWithForecast = [City]()
            
            for cityName in listCitiesArray {
                let stringForHttp = urlOpenWeatherMap + "=" + cityName + ",uk" + apiKey
                if let stringUrl = URL(string: stringForHttp) {
                    let weatherData = NSData(contentsOf: stringUrl)
                    do {
                        let weatherJson = try JSONSerialization.jsonObject(with: weatherData as! Data, options: []) as! NSDictionary
                        let city = City(dict: weatherJson)
                        newBunchOfCitiesWithForecast.append(city)
                    } catch  {
                        print(error)
                    }
                }
            }
            closureSucces(newBunchOfCitiesWithForecast)
        
        } else {
            print("check Internet connection")
            closureFailure()
        }
    }
    
    static func wallPostWithMessage (message: String, closure:@escaping () -> (Void)) {
        let  request:VKRequest = VKRequest(method: "wall.post",
                                           parameters: [VK_API_OWNER_ID    :VKSdk.accessToken().userId,
                                                        "message"          :message,
                                                        VK_API_ACCESS_TOKEN:VKSdk.accessToken().accessToken,
                                                        "v"                :"5.59"
                                           ])
        request.waitUntilDone = true
        request.execute(resultBlock: { (response) in
            closure()
        }) { (error) in
            print(error ?? "error")
        }
    }
}
