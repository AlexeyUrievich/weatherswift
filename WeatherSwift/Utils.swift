//
//  Utils.swift
//  WeatherSwift
//
//  Created by Alex on 29.01.17.
//  Copyright © 2017 Alex. All rights reserved.
//

import Foundation
import UIKit

class Utils {
    
    static func makeAnimation(cell: CityCell)  {
        let myView = cell.contentView
        myView.alpha = 0.0
        let scale = CGAffineTransform(scaleX: 0, y: 0)
        myView.transform = scale
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: [],
                       animations: {
                            myView.alpha = 1.0
                            let scale = CGAffineTransform(scaleX: 1, y: 1)
                            myView.transform = scale
        },
                       completion: nil)
    }
}
